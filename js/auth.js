"use strict";

/**
 * Object to handle user authentication
 * @constructor
 */
let Auth 									= function()
{
	// Get the elements
	let authScreens 						= document.getElementById("auth-screens");
	let appScreen 							= document.getElementById("app-screen");

	let screenLogin 						= document.getElementById("screen-login");
	let screenRegister 						= document.getElementById("screen-register");

	let loginForm 							= document.getElementById("form-login");
	let registerForm 						= document.getElementById("form-register");

	let showLoginBtn						= document.getElementById("show-login-btn");
	let showRegisterBtn						= document.getElementById("show-register-btn");

	let loginError 							= document.getElementById("login-error");
	let loginEmail 							= document.getElementById("login-email");
	let loginPassword 						= document.getElementById("login-password");
	let loginBtn 							= document.getElementById("login-btn");

	let registerError 						= document.getElementById("register-error");
	let registerName 						= document.getElementById("register-name");
	let registerEmail 						= document.getElementById("register-email");
	let registerPassword 					= document.getElementById("register-password");
	let registerBtn 						= document.getElementById("register-btn");

	let loginCallback 						= null;

	// Add event listeners
	showLoginBtn.addEventListener("click", toggleLoginRegister);
	showRegisterBtn.addEventListener("click", toggleLoginRegister);
	loginBtn.addEventListener("click", login);
	registerBtn.addEventListener("click", register);

	/**
	 * Function to set the action to perform once user logged in
	 * @param callback The function to call
	 */
	this.setLoginAction 					= function(callback)
	{
		loginCallback 						= callback;
	};

	/**
	 * Toggles the login/register form
	 */
	function toggleLoginRegister()
	{
		Helper.toggleFade(screenLogin, "fadeOut");
		Helper.toggleFade(screenRegister, "fadeIn");

		loginForm.reset();
		registerForm.reset();
	}

	/**
	 * Logs the user in
	 */
	function login()
	{
		// The login data
		let data 							= {
			"email"			: loginEmail.value,
			"password"		: loginPassword.value
		};

		// Send the login API request
		ApiInterface.instance.send("auth", "login", "login", data, result => {
			if(!result.success)
			{
				loginError.innerHTML 		= result.content.message;
				loginError.classList.add("fadeIn");
			}
			else
			{
				// Store the auth token in localstorage
				localStorage.setItem("auth-token", result.content.token);

				if(loginCallback != null)
					loginCallback();

				Helper.fadeOut(authScreens);
				Helper.fadeIn(appScreen);
			}
		});
	}

	/**
	 * Registers a user
	 */
	function register()
	{
		// The register data
		let data 							= {
			"name"			: registerName.value,
			"email"			: registerEmail.value,
			"password"		: registerPassword.value
		};

		// Send the register API request
		ApiInterface.instance.send("auth", "register", "register", data, result => {
			if(!result.success)
			{
				registerError.innerHTML 	= result.content.message;
				registerError.classList.add("fadeIn");
				registerError.classList.remove("success");
			}
			else
			{
				registerError.innerHTML 	= result.content.message;
				registerError.classList.add("fadeIn");
				registerError.classList.add("success");
			}
		});
	}
};