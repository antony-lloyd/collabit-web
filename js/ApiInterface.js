/**
 * Singleton class to create a WebSocket API interface
 * @constructor
 */
class ApiInterface
{
	constructor()
	{
		// Share the API instance
		if(!ApiInterface.instance)
		{
			this.websocket 					= new WebSocket("ws://collabitapp.co.uk:8080");
			this._socketCallback 			= null;
			this._filterCallbacks			= { };

			// Check if the WebSocket is closed, if so, show a error message
			window.setInterval(() => {
				if(this.websocket.readyState == this.websocket.CLOSED)
					Helper.showAlert(document.getElementById("alert-no-server"));
			}, 2000);

			/**
			 * Function to retrieve messages
			 * @param event
			 */
			this.websocket.onmessage 		= function(event)
			{
				if(event.data == "")
					return;

				let data 					= JSON.parse(event.data);

				let filter 					= data.content["filter"];
				let filterCallback			= ApiInterface.instance._filterCallbacks[filter];

				// Check if there is a callback filter function to call
				if (filterCallback != undefined)
					filterCallback[0](data, filterCallback[1]);
				else
				{
					// If a callback is defined, call it
					if (ApiInterface.instance._socketCallback != null)
						ApiInterface.instance._socketCallback(data);
				}
			};

			/**
			 * Function to handle WebSocket errors
			 */
			this.websocket.onerror 			= function()
			{
				Helper.showAlert(document.getElementById("alert-no-server"));
			};

			// Create the singleton instawnce
			ApiInterface.instance 			= this;
		}

		return ApiInterface.instance;
	}

	/**
	 * Set the socket callback function
	 * @param func
	 */
	set socketCallback(func)
	{
		this._socketCallback = func;
	}

	/**
	 * Send a API request
	 * @param api The API to call
	 * @param action The action to call
	 * @param method The action method to call
	 * @param data The request data
	 * @param callback Function to call on complete
	 */
	send(api, action, method, data, callback)
	{
		this.socketCallback 				= callback;

		let json 							= {
			"api"		: api,
			"action"	: action,
			"method"	: method,
			"data"		: data,
			"token"		: localStorage.getItem("auth-token")
		};

		this.websocket.send(JSON.stringify(json));
	};

	/**
	 * Adds a new filter callback for filtering responses
	 * @param filter The filter string
	 * @param callback The callback function
	 * @param parentInstance The parent class instance of the callback
	 */
	addFilterCallback(filter, callback, parentInstance)
	{
		this._filterCallbacks[filter] 		= [ callback, parentInstance ];
	}
}