"use strict";
/**
 * Function to setup the auth and home class on page load
 */
document.addEventListener("DOMContentLoaded", function()
{
	// Initalise the API
	let api 								= new ApiInterface();

	let auth 								= new Auth();
	let home 								= new Home();

	// Set the action to perform once the user has logged in
	auth.setLoginAction(() => {
		home.loadRecientSessions();
	});
});