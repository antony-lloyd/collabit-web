"use strict";

/**
 * Class for handling the home screen of the app
 * @constructor
 */
let Home 									= function()
{
	let editController 						= null;

	// Get the elements
	let appScreen 							= document.getElementById("app-screen");
	let newScreen 							= document.getElementById("new-session-screen");
	let joinScreen 							= document.getElementById("join-session-screen");
	let editScreen 							= document.getElementById("edit-screen");

	let newSessionBtn 						= document.getElementById("show-create-session-btn");
	let cancelNewSessionBtn 				= document.getElementById("cancel-create-session-btn");
	let createSessionBtn 					= document.getElementById("create-session-btn");
	let newSessionName 						= document.getElementById("new-session-name");

	let showJoinSessionBtn 					= document.getElementById("show-join-session-btn");
	let cancelJoinSessionBtn 				= document.getElementById("cancel-join-session-btn");
	let joinSessionBtn						= document.getElementById("join-session-btn");
	let joinSessionCode 					= document.getElementById("join-session-code");

	let sessionsContainer 					= document.getElementById("sessions-container");

	let deleteSessionAlert 					= document.getElementById("alert-delete-session");
	let deleteSessionAlertNo 				= document.getElementById("alert-delete-session-no");
	let deleteSessionAlertYes 				= document.getElementById("alert-delete-session-yes");
	let deleteSessionAlertId 				= document.getElementById("alert-delete-session-id");

	// Setup event listeners
	let parent 								= this;
	newSessionBtn.addEventListener("click", toggleNewSession);
	createSessionBtn.addEventListener("click", createSession);
	cancelNewSessionBtn.addEventListener("click", toggleNewSession);
	showJoinSessionBtn.addEventListener("click", toggleJoinSession);
	cancelJoinSessionBtn.addEventListener("click", toggleJoinSession);
	joinSessionBtn.addEventListener("click", () => joinSession(joinSessionCode.value, parent));

	// Delete session buttons event listener
	deleteSessionAlertNo.addEventListener("click", () => Helper.hideAlert(deleteSessionAlert));
	deleteSessionAlertYes.addEventListener("click", () => {
		// Send the delete request
		ApiInterface.instance.send("session", "session", "delete", { code : deleteSessionAlertId.value }, result => {
			// Hide the alert, but override the popup container fade out
			Helper.hideAlert(deleteSessionAlert, !result.success);

			// If delete was successful, reload the sessions
			if(result.success == true)
			{
				this.loadRecientSessions();
			}
			else
			{
				let alert 					= Helper.createAlert("delete-failed", "The session failed to delete", {
					text : "OK",
					click : () => Helper.destroyAlert(alert)
				});
				Helper.showAlert(alert);
			}
		});
	});

	/**
	 * Loads the list of recient user sessions
	 */
	this.loadRecientSessions 				= function()
	{
		// Send the request
		ApiInterface.instance.send("session", "session", "getAll", [ ], result => {
			let sessions 					= result.content.sessions;

			// Remove existing sessions
			while(sessionsContainer.hasChildNodes())
				sessionsContainer.removeChild(sessionsContainer.lastChild);

			// Create buttons for each session
			for(let session of sessions)
			{
				// Create the session button, the flip container
				let sessionButton 			= document.createElement("article");
				sessionButton.classList.add("flip-container");

				// Create the flippable content
				let flipContent 			= document.createElement("div");
				flipContent.classList.add("flip-flipper");

				// The front face of the flipper
				let frontFace 				= document.createElement("div");
				frontFace.classList.add("flip-face", "front");

				// The front face content
				let sessionImage 			= document.createElement("img");
				sessionImage.src 			= "data:image/jpeg;base64," + session.preview;
				frontFace.appendChild(sessionImage);

				if(session.preview == "")
					sessionImage.style.display = "none";

				// The back face of the flipper
				let backFace 				= document.createElement("div");
				backFace.classList.add("flip-face", "back");

				// The back face content
				let openBtn 				= document.createElement("button");
				openBtn.classList.add("action");
				openBtn.innerText 			= "Open";
				openBtn.setAttribute("data-session-code", session.code);
				openBtn.addEventListener("click", (e) => joinSession(e.target.getAttribute("data-session-code"), parent));

				let deleteBtn 				= document.createElement("button");
				deleteBtn.classList.add("action", "red");
				deleteBtn.innerText 		= "Delete";
				deleteBtn.setAttribute("data-session-code", session.code);
				deleteBtn.addEventListener("click", e => {
					deleteSessionAlertId.value = e.target.getAttribute("data-session-code");
					Helper.showAlert(deleteSessionAlert);
				});

				backFace.appendChild(openBtn);
				backFace.appendChild(deleteBtn);

				// Add the faces to the flipper
				flipContent.appendChild(frontFace);
				flipContent.appendChild(backFace);

				sessionButton.appendChild(flipContent);

				let sessionTitle 			= document.createElement("div");
				sessionTitle.innerHTML 		= `<h5>${session.name}</h5>`;
				sessionButton.appendChild(sessionTitle);

				sessionsContainer.insertBefore(sessionButton, sessionsContainer.firstChild);
			}
		});
	};

	/**
	 * Toggles the new session screen
	 */
	function toggleNewSession()
	{
		Helper.toggleFade(appScreen, "fadeOut");
		Helper.toggleFade(newScreen, "fadeIn");
	}

	/**
	 * Toggles the join session screen
	 */
	function toggleJoinSession()
	{
		Helper.toggleFade(appScreen, "fadeOut");
		Helper.toggleFade(joinScreen, "fadeIn");
	}

	/**
	 * Function to join a editing session
	 * @param sessionCode The share code of the session to join
	 * @param homeInstance The current home class instance
	 */
	function joinSession(sessionCode, homeInstance)
	{
		// Send the request
		ApiInterface.instance.send("session", "session", "join", { "code" : sessionCode }, result => {
			// If successful, show the edit screen
			if(result.success == true)
			{
				// Create a new edit class
				editController 				= new Edit(result.content["session-code"], result.content["session-name"]);
				// Set the action to close on exit
				editController.setOnCloseAction(() => {
					homeInstance.loadRecientSessions();
					Helper.fadeOut(editScreen);
					// Wait 200ms due to absolute positioning
					setTimeout(() => {
						Helper.fadeIn(appScreen);
					}, 200);
				});

				// Load the previous collaboration image
				editController.loadImage(result.content["session-image"]);

				// Hide the home screens, show the edit screen
				Helper.fadeOut(joinScreen);
				Helper.fadeOut(appScreen);
				Helper.fadeIn(editScreen);
			}
		});
	}

	/**
	 * Creates a session
	 */
	function createSession()
	{
		ApiInterface.instance.send("session", "session", "create", { "name" : newSessionName.value }, result => {
			// If the creation was a success, join it
			if(result.success == true)
			{
				Helper.fadeOut(newScreen);
				joinSession(result.content.code, parent);
			}
		});
	}
};