/**
 * Functions to provide visual functionality
 */
let Helper 					= {
	_openAlertCount 		: 0, // Count of the open alert
	/**
	 * Toggles a fade on a element
	 * @param element The element to toggle
	 * @param defaultFade The default fade to use if one isn't already applied
	 */
	toggleFade 				: function(element, defaultFade)
	{
		// If the element does not currently contain a fade event, add the default
		if(!element.classList.contains("fadeOut") && !element.classList.contains("fadeIn"))
		{
			element.classList.add(defaultFade);
			return;
		}

		if(element.classList.contains("fadeOut"))
			Helper.fadeIn(element);
		else if(element.classList.contains("fadeIn"))
			Helper.fadeOut(element);
	},
	/**
	 * Fades in a element
	 * @param element The element to fade in
	 */
	fadeIn 					: function(element)
	{
		if(element.classList.contains("fadeIn"))
			return;

		element.style.display = "block";
		element.style.opacity = 0;
		element.classList.remove("fadeOut");
		element.classList.add("fadeIn");
	},
	/**
	 * Fades out a element
	 * @param element The element to fade out
	 */
	fadeOut 				: function(element)
	{
		if(element.classList.contains("fadeOut"))
			return;

		element.style.visibility = "hidden";
		element.classList.remove("fadeIn");
		element.classList.add("fadeOut");
		setTimeout(() => element.style.display = "none", 200);
	},
	/**
	 * Shows a pre-created alert dialog
	 * @param alert The alert to show
	 */
	showAlert 				: function(alert)
	{
		// If no alerts are visible, fade in the container and apply the blur in animation
		if(Helper._openAlertCount == 0)
		{
			Helper.fadeIn(document.getElementById("popup-container"));
			document.getElementById("blur-container").style.animation = "fadeblurin 0.2s forwards";
		}
		Helper._openAlertCount++;

		Helper.fadeIn(alert);
	},
	/**
	 * Hides an alert dialog
	 * @param alert The alert to hide
	 * @param overrideContainer If true, prevents the popup container from fading out
	 */
	hideAlert 				: function(alert, overrideContainer)
	{
		Helper._openAlertCount--;
		// If no alerts a visible, fade out the container. Unless overridden
		if(Helper._openAlertCount == 0 && overrideContainer !== true)
		{
			Helper.fadeOut(document.getElementById("popup-container"));
			document.getElementById("blur-container").style.animation = "fadeblurout 0.2s forwards";
		}

		Helper.fadeOut(alert);
	},
	/**
	 * Hides and removes an alert dialog
	 * @param alert The alert to remove
	 */
	destroyAlert 			: function(alert)
	{
		Helper.hideAlert(alert);
		setTimeout(() => document.getElementById("popup-container").removeChild(alert), 200);
	},
	/**
	 * Creates an alert dialog
	 * @param id The alert Id
	 * @param text The alert text
	 * @param button1Opts The alert button 1 options. { text : "(string) Button text", red : "(bool) If the button is red", click : "Function to call on click" }
	 * @param button2Opts The alert button 2 options. { text : "(string) Button text", red : "(bool) If the button is red", click : "Function to call on click" }
	 * @returns {Element}
	 */
	createAlert 			: function(id, text, button1Opts, button2Opts)
	{
		// Create the container
		let container 		= document.createElement("div");
		container.setAttribute("id", `alert-${id}`);

		// Create the text heading
		let heading 		= document.createElement("h4");
		heading.innerText 	= text;
		container.appendChild(heading);

		// If button 1 options provided, create the button
		if(button1Opts !== undefined && button1Opts !== null)
		{
			let button1 	= document.createElement("button");
			button1.innerText = button1Opts.text;
			button1.classList.add("action");
			if(button1Opts.red !== undefined && button1Opts.red == true)
				button1.classList.add("red");
			button1.addEventListener("click", button1Opts.click);
			container.appendChild(button1)
		}

		// If button 2 options provided, create the button
		if(button2Opts !== undefined && button2Opts !== null)
		{
			let button2 	= document.createElement("button");
			button2.innerText = button1Opts.text;
			button2.classList.add("action");
			if(button2Opts.red !== undefined && button2Opts.red == true)
				button2.classList.add("red");
			button2.addEventListener("click", button2Opts.click);
			container.appendChild(button2)
		}

		// Add the alert
		document.getElementById("popup-container").appendChild(container);
		return container;
	}
};