/**
 * Class to handle the collaborative editing using canvas
 */
class Edit
{
	/**
	 * Constructor
	 * @param sessionCode The session share code
	 * @param sessionName The session name
	 */
	constructor(sessionCode, sessionName)
	{
		// Register a callback to retrieve shared edits from other clients
		ApiInterface.instance.addFilterCallback("session-edit", this._onMessage, this);

		// Get the elements
		this.editScreen 					= document.getElementById("edit-screen");

		this.titleHeader 					= document.getElementById("edit-screen-session-name");
		// this.shareCodeHeader 				= document.getElementById("edit-screen-share-code");
		this.canvas 						= document.getElementById("edit-screen-canvas");
		this.exitBtn 						= document.getElementById("edit-screen-close");
		this.saveBtn 						= document.getElementById("edit-screen-save");
		this.penColour 						= document.getElementById("edit-screen-pen-colour");
		this.penErase 						= document.getElementById("edit-screen-pen-erase");
		this.shareCodeAlert 				= document.getElementById("alert-session-code");
		this.shareCodeAlertValue 			= document.getElementById("alert-session-code-value");
		this.shareCodeAlertOk 				= document.getElementById("alert-session-code-ok");
		this.infoBtn 						= document.getElementById("edit-screen-info");

		// A function to call when the editing session is closing
		this._onCloseAction					= null;

		// Setup the canvas
		this.canvasContext 					= this.canvas.getContext("2d");
		// Give the canvas a white background
		this.canvasContext.fillStyle = "rgb(255, 255, 255)";
		this.canvasContext.fillRect(0, 0, this.canvas.width, this.canvas.height);
		this._setCanvasSize(this);

		// Buffer for other client edits
		this._clientEditBuffer 				= [ ];

		// Drawing variables
		this.drawing 						= {
			swipe 		: false,
			prev 		: { x : 0, y : 0 },
			colour 		: {
				red 	: 0,
				green	: 0,
				blue	: 0
			},
			erase 		: false,
			weight 		: 2
		};

		// Set the canvas listeners
		var parent 							= this;
		this.canvas.addEventListener("mousedown", 	e => parent._canvasClick(e, parent));
		this.canvas.addEventListener("mousemove", 	e => parent._canvasMove(e, parent));
		this.canvas.addEventListener("mouseup", 	e => parent._canvasRelease(e, parent));
		this.canvas.addEventListener("mouseleave", 	e => parent._canvasLeave(e, parent));
		this.canvas.addEventListener("touchstart",	e => parent._canvasClick(e, parent));
		this.canvas.addEventListener("touchmove", 	e => parent._canvasMove(e, parent));
		this.canvas.addEventListener("touchend", 	e => parent._canvasRelease(e, parent));
		this.canvas.addEventListener("touchcancel", e => parent._canvasLeave(e, parent));

		// On window resize, update the canvas
		window.addEventListener("resize", () => parent._setCanvasSize(parent));

		// The exit button event
		this.exitBtn.addEventListener("click", () => {
			// Save the session then call the onclose action
			parent._saveSession(parent, (result) => parent._onCloseAction());

			// Wait 200ms before resetting the canvas
			setTimeout(() => {
				parent.canvasContext.clearRect(0, 0, parent.canvas.width, parent.canvas.height);
				parent._setCanvasSize(parent);
			}, 200);
		});

		// The save button event
		this.saveBtn.addEventListener("click", () => parent._saveSession(parent));

		this.infoBtn.addEventListener("click", () => Helper.showAlert(parent.shareCodeAlert));

		this.shareCodeAlertOk.addEventListener("click", () => Helper.hideAlert(parent.shareCodeAlert));

		// Pen colour event
		this.penColour.addEventListener("click", e => {
			parent.drawing.erase 			= false;
		});
		this.penColour.addEventListener("change", e => {
			let hex 						= e.target.value;
			parent.drawing.colour.red 		= parseInt(hex.substring(0, 2), 16);
			parent.drawing.colour.green 	= parseInt(hex.substring(2, 4), 16);
			parent.drawing.colour.blue		= parseInt(hex.substring(4, 6), 16);
			parent.drawing.erase 			= false;
		});

		this.penErase.addEventListener("click", e => {
			parent.drawing.erase 				= true;
		});

		// Show the session name and share code
		this.titleHeader.innerHTML 			= sessionName;
		this.shareCodeAlertValue.innerText 	= `Share code for this session: ${sessionCode}`;
	}

	/**
	 * Sets the function to call when the editing session is closing
	 * @param action The callback function
	 */
	setOnCloseAction(action)
	{
		this._onCloseAction 				= action;
	}

	/**
	 * Loads a image onto the canvas
	 * @param image The base64 encoded image string
	 */
	loadImage(image)
	{
		let imageObject 					= new Image();
		imageObject.onload 					= () => {
			this._setCanvasSize(this);
			this.canvasContext.drawImage(imageObject, 0, 0);
		};
		imageObject.src 					= "data:image/jpeg;base64," + image;
	}

	/**
	 * Draws a line from the previous coords to the new coord
	 * @param from The from pos
	 * @param to The to pos
	 * @param colour The pen RGB colour { red : 0, green : 0, blue : 0 }
	 * @param erase Sate if to erase
	 * @param move State if the mouse is moving
	 * @param sharedEdit State if the line is from a client
	 */
	drawLine(from, to, colour, erase, move, sharedEdit)
	{
		// If moving, draw the line
		if(move)
		{
			this.canvasContext.beginPath();
			this.canvasContext.strokeStyle 	= `rgb(${colour.red},${colour.green},${colour.blue})`;
			this.canvasContext.lineWidth 	= this.drawing.weight;
			this.canvasContext.lineJoin 	= "round";
			this.canvasContext.moveTo(from.x, from.y);
			this.canvasContext.lineTo(to.x, to.y);

			if(erase)
			{
				// This gives the correct but introduces transparancy
				// as the server uses JPEG, it will be replaced with black.
				// Setting the pen to white will give the correct illusion.
				// this.canvasContext.globalCompositeOperation = "destination-out";
				this.canvasContext.strokeStyle = "rgba(255, 255, 255, 1)";
			}
			else
				this.canvasContext.globalCompositeOperation = "";

			this.canvasContext.closePath();

			this.canvasContext.stroke();
		}

		// If a shared edit, do not send it as an update
		if(sharedEdit)
			return;

		// Send the line to other clients
		ApiInterface.instance.send("session", "edit", "update", {
			"page" 		: 1,
			"colour" 	: [ this.drawing.colour.red, this.drawing.colour.green, this.drawing.colour.blue ],
			"erase" 	: this.drawing.erase,
			"weight" 	: this.drawing.weight,
			"point" 	: {
				"from"	: [ from.x, from.y ],
				"to" 	: [ to.x, to.y ]
			}
		});
	}

	/**
	 * Function called when the canvas is clicked/pressed
	 * @param e The click/touch event
	 * @param editInstance The parent edit class instance
	 * @private
	 */
	_canvasClick(e, editInstance)
	{
		editInstance.drawing.swipe 			= true;
		editInstance.drawing.prev.x 		= e.pageX - editInstance.canvas.offsetLeft;
		editInstance.drawing.prev.y			= e.pageY - editInstance.canvas.offsetTop;
		editInstance.drawLine(editInstance.drawing.prev, editInstance.drawing.prev, editInstance.drawing.colour, editInstance.drawing.erase, false, false);
	}

	/**
	 * Function called when the canvas is clicked/pressed and dragged
	 * @param e The move event
	 * @param editInstance The parent edit class instance
	 * @private
	 */
	_canvasMove(e, editInstance)
	{
		if(editInstance.drawing.swipe)
		{
			let to 							= {
				x : e.pageX - editInstance.canvas.offsetLeft,
				y : e.pageY - editInstance.canvas.offsetTop
			};
			editInstance.drawLine(editInstance.drawing.prev, to, editInstance.drawing.colour, editInstance.drawing.erase, true, false);

			editInstance.drawing.prev 		= to;
		}
	}

	/**
	 * Function called when the canvas is released
	 * @param e The release event
	 * @param editInstance The parent edit class event
	 * @private
	 */
	_canvasRelease(e, editInstance)
	{
		editInstance.drawing.swipe 			= false;

		// If there are any edits from other clients stored, draw them
		for(let edit of editInstance._clientEditBuffer)
		{
			editInstance._drawBuffer(edit, editInstance);
		}
	}

	/**
	 * Function called when the touch event leaves the canvas
	 * @param e The event
	 * @param editInstance The parent edit class event
	 * @private
	 */
	_canvasLeave(e, editInstance)
	{
		editInstance.drawing.swipe		 	= false
	}

	/**
	 * Sets the canvas size
	 * @param editInstance The class instance
	 * @private
	 */
	_setCanvasSize(editInstance)
	{
		let tmpImage 						= editInstance.canvasContext.getImageData(0, 0, editInstance.canvas.width, editInstance.canvas.height);

		editInstance.canvas.width 			= editInstance.editScreen.offsetWidth - 30;
		editInstance.canvas.height 			= editInstance.editScreen.offsetHeight - 85;

		// Give the canvas a white background
		editInstance.canvasContext.fillStyle = "rgb(255, 255, 255)";
		editInstance.canvasContext.fillRect(0, 0, editInstance.canvas.width, editInstance.canvas.height);

		editInstance.canvasContext.putImageData(tmpImage, 0, 0);
	}

	/**
	 * Retrieves messages from other clients
	 * @param data The message data
	 * @param instance The class instance
	 * @private
	 */
	_onMessage(data, instance)
	{
		// If the host is not drawing, draw the edit. Otherwise, add it to the buffer
		if(instance.drawing.swipe === false)
			instance._drawBuffer(data, instance);
		else
		{
			// Add the edit to the buffer
			instance._clientEditBuffer.push(data);
		}
	}

	/**
	 * Draws any data from other clients that is stored in the buffer
	 * @param data The buffer data to draw
	 * @param instance The current edit instance
	 * @private
	 */
	_drawBuffer(data, instance)
	{
		// Get the edit data
		let colour 							= data["content"]["colour"];
		let erase 							= data["content"]["erase"];
		let weight 							= data["content"]["weight"];
		let fromPoint 						= data["content"]["point"]["from"];
		let toPoint 						= data["content"]["point"]["to"];

		// Convert the colour values if needed, as the iOS app values a divided by 255
		let correctValue 					= {
			red 	: (colour[0] < 1 && colour[0] != 0) ? Math.round(colour[0] * 255) : colour[0],
			green 	: (colour[1] < 1 && colour[1] != 0) ? Math.round(colour[1] * 255) : colour[1],
			blue 	: (colour[2] < 1 && colour[2] != 0) ? Math.round(colour[2] * 255) : colour[2]
		};

		// Draw the line
		instance.drawLine({ x : fromPoint[0], y : fromPoint[1] }, { x : toPoint[0], y : toPoint[1] }, correctValue, erase, true, true);
	}

	/**
	 * Saves the current editing session to the server
	 * @param editInstance The current edit instance
	 * @param callback The function to call after save
	 * @private
	 */
	_saveSession(editInstance, callback)
	{
		ApiInterface.instance.send("session", "edit", "save", {
			image 	: encodeURI(editInstance.canvas.toDataURL("image/jpeg").split(",")[1])
		}, callback);
	}
}