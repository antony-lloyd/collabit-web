# CollabIt ReadMe
## URLs
__Live URL__: http://collabitapp.co.uk

## Account credentials
Test accounts for the ws://collabitapp.co.uk server have already been generated, new accounts can also be generated.

|Email        |Password|
|-------------|--------|
|jdoe@me.com|abc123|
|sjones@me.com|qwe123|

## About the site
CollabIt is a HTML5 web app that utilizes HTML5 canvas and WebSockets to allow real time collaborative drawing between multiple concurrent users.

## The WebSocket server
The web app uses a WebSocket server written in PHP7 to handle the collaboration between users and uses JSON to transfer data between the clients. The server can be access at the address ws://collabitapp.co.uk:8080. The server can also be ran locally using the code from the server git repo: https://bitbucket.org/antony-collabit/collabit-server. If so, the connection address will need to be updated in ApiInterface.js. If the app fails to connect or looses connection with the server, a notification message will be displayed.

## Issues and improvements
* The collaborative editing makes use of one canvas element. Because of this any edits retrieved from any other client needs to be stored in a buffer if the user is currently drawing. If immediatly drawn the two will clash and will produce the incorrect result. A potential fix for this could be to include a extra canvas for external editing overlapped with the users canvas.
* Resizing of the window or small screens can hide content. Upon resizing the window to a smaller size it can hide content whilst still remaining functional. This is due to the drawing points using absoulte pixel positioning. Fixes to this issue could include  scaling the content of the canvas, or allowing the canvas to be scrollable to reveal hidden content.
* Mobile browsers and the touch to scroll feature. Mobile web browsers allow web pages to be scrolled by using a single finger. This interfears with the canvas drawing as moving a finger on the canvas will also scroll the window, even if there is no overflow. The ultimate solution for this is to provide a native mobile application but there may be some JavaScript that could eliminate this feature.
* A small snippet of PHP could be utilized to allow a 'remember me' function. This could not be included with a all JavaScript version as there is no save way to store user credentials, although local storage is used to remember the user API JSON web token.
* Internet Explorer. The web app fails to work using Internet Explorer 11.